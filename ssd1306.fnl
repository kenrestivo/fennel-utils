(local lume (require :lume))

(local ssd-1306 {:activate-scroll   0x2F
                 :data 0x40
                 :charge-pump   0x8D
                 :column-addr   0x21
                 :com-scan-dec   0xC8
                 :com-scan-inc   0xC0
                 :deactivate-scroll 0x2E
                 :display-all-on   0xA5
                 :display-all-on-resume   0xA4
                 :display-off   0xAE
                 :display-on   0xAF
                 :external-vcc   0x01
                 :invert-display   0xA7
                 :left-horizontal-scroll 0x27
                 :memory-mode   0x20
                 :normal-display   0xA6
                 :page-addr   0x22
                 :right-horizontal-scroll   0x26
                 :seg-remap-0   0xA0
                 :seg-remap-127   0xA1
                 :set-com-pins   0xDA
                 :set-contrast   0x81
                 :set-display-clockdiv   0xD5
                 :set-display-offset   0xD3
                 :set-high-column   0x10
                 :set-low-column   0x00
                 :set-multiplex   0xA8
                 :set-precharge   0xD9
                 :set-startline   0x40
                 :set-vcom-deselect   0xDB
                 :set-vertical-scroll-area   0xA3 
                 :switch-cap-vcc   0x02
                 :vertical-and-left-horizontal-scroll   0x2A
                 :vertical-and-right-horizontal-scroll   0x29})



(fn reverse-translate-all
  [seqs]
  (let [revd (lume.invert ssd-1306)]
    (lume.map seqs (fn fix-bytes
                     [bytes]
                     (lume.map bytes (fn fix-byte
                                       [byte]
                                       (or (. revd byte) byte)))))))


(fn translate-block
  [bytes]
  (lume.map bytes (fn fix-byte
                    [byte]
                    (or (. ssd-1306 byte) byte))))


(local setup-commands [[0x00 :display-off
                        :set-display-clockdiv 0x80
                        :set-multiplex]
                       [0x00 0x1F] ; height - 1 !!
                       
                       [0x00 :set-display-offset 0x00
                        :set-startline
                        :charge-pump]
                       [0x00 0x14] ; externalvc state?
                       
                       [0x00 :memory-mode 0x00
                        :seg-remap-127 ; why?
                        :com-scan-dec]
                       [0x00 :set-com-pins]
                       [0x00 0x02] ; this is the fucking compins value! for above!
                       [0x00 :set-contrast]
                       [0x00 0x8F] ; the contrast value!
                       
                       [0x00 :set-precharge]
                       [0x00 0xF1]
                       
                       [0x00 :set-vcom-deselect 
                        0x40 ; ???
                        :display-all-on-resume
                        :normal-display
                        :deactivate-scroll
                        :display-on]])


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; examples
;; refresh display!
;; (local reset-page-col-addrs-commands [[0x00 :page-addr 0x00 0xFF] ; start and end
;;                                       [0x00 :column-addr 0x00 0x7F]])

;; (send-blocks! f-num (translate-all setup-commands))

;; (send-blocks! f-num (translate-all reset-page-col-addrs-commands))
;; (send-blocks! f-num (translate-all blank-screen-commands))

;; (send-blocks! f-num (translate-all reset-page-col-addrs-commands))
;; (send-blocks! f-num (translate-all simple-star-logo))
