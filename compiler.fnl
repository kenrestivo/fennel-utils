;; I did not write this, but no idea wher it came from

(fn compile [args]
  (let [fennel (require :fennel)
        fennelview (require :fennelview)
        (ok val) (pcall fennel.compileString args {:indent ""})
        oneline-val (when ok (string.gsub val "\n" " "))]
    (if ok oneline-val (.. "Error" val))))
