;; ken misc fennel utils
;; EPL ken restivo<ken@restivo.org>

(global http (require "socket.http"))
(global json (require "json"))



(defn rest-get
    [url]
  (let [(r x) (http.request url)]
    ;; TODO check for nil error val
    (json.decode r)))



(defn safe-str
    [v]
  (if (or (= :string (type v))
          (= :number (type v))) 
      v
      ""))

