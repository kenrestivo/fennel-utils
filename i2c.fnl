(global ffi (require :ffi))
(local libi2c (ffi.load :libi2c))
(local lume (require :lume))

(ffi.cdef "
    int ioctl(int fd, unsigned long request, ...);
    int fileno(void *stream);

    typedef enum _i2c_consts {
       I2C_RDWR_IOCTL_MAX_MSGS  = 42,
       I2C_RETRIES  =  0x0701,
       I2C_TIMEOUT,
       I2C_SLAVE,
       I2C_TENBIT,
       I2C_FUNCS,
       I2C_SLAVE_FORCE,
       I2C_RDWR,
       I2C_PEC,
       I2C_SMBUS  = 0x0720
    } i2c_consts_t;
   ")




(ffi.cdef "
int32_t i2c_smbus_write_i2c_block_data(int file, uint8_t command, uint8_t length,uint8_t *values);
")



(ffi.cdef "
char *strerror(int errnum);
int printf(const char *format, ...);
")




(fn translate-all
  [seqs]
  (lume.map seqs translate-block))


(fn array-to-bytes
  [bs]
  (ffi.new "uint8_t[?]" (# bs) bs))

(fn check-res
  [res]
  (if (= 0 res)
      res
      (-> res
          math.abs
          ffi.C.strerror
          ffi.string)))



(fn send-block!
  [f-num [cmd & vals]]
  (print "sending" cmd (# vals) (. vals 1))
  (let [p (array-to-bytes vals)]
    ;;(print "" "bytes" (. p 0) (. p 1))
    (check-res (libi2c.i2c_smbus_write_i2c_block_data f-num cmd (# vals) p))))


(fn send-blocks!
  [f-num seqs]
  (each [_ c (ipairs seqs)]
    (send-block! f-num c)))


(fn setup!
  [i2c-dev-path device-address]
  (let [f-handle (io.open i2c-dev-path "w")
        f-num (ffi.C.fileno f-handle)]
    (check-res (ffi.C.ioctl f-num ffi.C.I2C_SLAVE (ffi.new "uint8_t"  device-address)))
    {:f-num f-num
     :file-handle f-handle}))



